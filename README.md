## JFinal2.0 beetl maven项目培训
我们有多年的JFinal、Spring开发经验。熟悉JFinal架构以及源码，有着丰富的实战经验，如梦技术期待你的加入！

## 面向的群体
1. 学生、初学者（入门、就业）
2. 中级学员，扎实技术，迈向大牛，寻求更高工资
3. 企业内部员工培训

## 课程介绍
>1. 第一章 简介 
>2. 第二章 JFinal2.0 maven demo和JFinal源码结构讲解
>3. 第三章 JFinal2.0 基础与加强
>4. 第四章 JFinal2.0 核心揭秘
>5. 第五章 JFinal2.0 项目实战

## 项目介绍
该项目为第三章和第四章的技术穿插，逐步将其打造成一个成熟的系统！

## 加入我们
课程视频地址：http://blog.dreamlu.net/blog/79

如梦技术QQ群：[`237587118`](http://shang.qq.com/wpa/qunwpa?idkey=f78fcb750b4f72c92ff4d375d2884dd69b552301a1f2681af956bd32700eb2c0)

如梦技术IT-专业的jfinal2.0学习培训平台


## 捐助共勉
<img src="http://soft.dreamlu.net/weixin-9.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/weixin-19.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/alipay.png" width = "200" alt="支付宝捐助" align=center />

<img src="http://soft.dreamlu.net/qq-9.jpg" width = "200" alt="QQ捐助" align=center />
<img src="http://soft.dreamlu.net/qq-19.jpg" width = "200" alt="QQ捐助" align=center />